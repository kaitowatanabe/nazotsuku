$(function() {
    $('.jquery-ui-sortable').sortable( {
        connectWith: '.jquery-ui-sortable'
    } );
    $( '.jquery-ui-sortable' ).disableSelection();
    $(document).on('click', '.addTrigger', function() {
      $(".jquery-ui-sortable").append('<li class="trigger"><h2>Trigger</h2>'
      +'<button class="addTrigger">+</button><button  class="deleteTrigger">-</button><ul class="clearfix">'
      +'<li><ul class="conditions"><li><select name="trigger"><option value="">条件を選択</option><option value="string">String</option>'
      +'<option value="beacon">Beacon</option><option value="timer">Timer</option></select><input type="text">'
      +'<button class="addCondition">+</button></li></ul></li><li class="contents">'
      +'<h2>Contents</h2><div><select name="trigger"><option value="">コンテンツを選択</option>'
      +'<option value="img">画像</option><option value="youtube">Youtube URL</option></select><input type="text"></div>'
      +'</li></ul></li>').children('ul').sortable({
        connectWith: '.jquery-ui-sortable'
      });
    });
    $(document).on('click', '.deleteTrigger', function(){
      $(this).parent().remove();
    });
    $(document).on('click', '.addCondition', function() {
      $(this).parent().parent().append('<li><select name="trigger"><option value="">条件を選択</option>'
      +'<option value="string">String</option><option value="beacon">Beacon</option><option value="timer">Timer</option>'
      +'</select><input type="text"><button class="addCondition">+</button><button class="deleteCondition">-</button></li>');
    });
    $(document).on('click', '.deleteCondition', function(){
      $(this).parent().remove();
    });
    var form = document.createElement("form");
    form.setAttribute("action", "/create");
    form.setAttribute("method", "post");
    form.style.display = "none";
    document.body.appendChild(form);

    $('.contents select').change(function(event) {
      if($(this).val() == "img"){
        alert('aaa');
        $(this).next('input').attr('type', 'file');
      }
    });

    $('#btn').click(function() {
      $('.trigger').each(function(triggerNo, el) {
        $(this).find('.conditions>li').each(function(conditionNo, el) {
          var condition = new Array();
          condition["type"] = $(this).children('select').val();
          condition["data"] = $(this).children('input').val();
          sendData(form,'conditions'+triggerNo+"-"+conditionNo,condition["type"]+","+condition["data"]);
        });
        sendData(form, "conditionLen"+triggerNo,$(this).find('.conditions>li').length);
        
        //$(this).find('.contents>li').each(function(contentNo, el) {
          var content = new Array();
          content["type"] = $(this).find('.contents>div').children('select').val();
          content["data"] = $(this).find('.contents>div').children('input').val();
          sendData(form,'contents'+triggerNo,content["type"]+","+content["data"]);
        //});
        if (triggerNo === $('.trigger').length -1 ) {
          sendData(form, "triggerLen",$('.trigger').length);
          form.submit();
        };
      });
    });
});

function sendData (form,name, data) {  
  var input = document.createElement('input');
  input.setAttribute('type', 'hidden');
  input.setAttribute('name', name);
  input.setAttribute('value', data);
  console.log(data);
  form.appendChild(input);
}
