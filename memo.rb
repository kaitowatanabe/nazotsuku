require 'mongo'
require 'uri'
require 'json'
require 'erb'

def mongo
  return @mongo if @mongo

  # configure connection via ENV['MONGODB_URI']
  # bash:
  #  export MONGODB_URI=mongodb://username:password@host:port/dbname
  # heroku:
  #  heroku config:set MONGODB_URI=mongodb://username:password@host:port/dbname
  client = Mongo::MongoClient.new
  @mongo = client.db
end

def packages
  mongo.collection("packages")
end

before do
  content_type = "application/json"
end

get '/' do
  erb :index
end

post '/create'  do
  timeline = []
  tl = params["triggerLen"].to_i
  for n in 0..(tl-1) do
    condl = params["conditionLen#{n}"].to_i
    conds = []
    for m in 0..(condl-1) do
      c = params["conditions#{n}-#{m}"]
      cond = {"type"=>c.split(",")[0], "data"=>c.split(",")[1]}
      conds.push(cond)
    end
    ct = params["contents#{n}"]
    cont = {"type"=>ct.split(",")[0], "data"=>ct.split(",")[1]}
    item = {"conditions" => conds, "contents" => cont}
    timeline.push(item);
    p timeline  
  end
  inserted_id = packages.insert(:timeline => timeline)
  jsonpar = JSON.parse(inserted_id.to_json)
  jsonpar["$oid"].to_s
   
end 

get '/:id' do
  id = BSON::ObjectId(params[:id])

  docs = packages.find(:_id => id).to_a
  halt 404, { :message => "not found" }.to_json if docs.none?

  docs[0].to_json
end

put '/memos/:id' do
  id = BSON::ObjectId(params[:id])

  docs = memos.find(:_id => id).to_a
  halt 404, { :message => "not found" }.to_json if docs.none?

  filter = ["title", "description"]
  update_params = params.select { |key, _| filter.include? key }
  memos.update({ :_id => id }, docs[0].merge(update_params))

  updated_docs = memos.find(:_id => id).to_a
  halt 500, { :message => "failed to update memo" }.to_json if updated_docs.none?

  updated_docs[0].to_json
end

delete '/memos/:id' do
  id = BSON::ObjectId(params[:id])

  memos.remove(:_id => id)
  halt 500, { :message => "failed to remove memo" }.to_json unless memos.find(:_id => id).to_a.none?

  { :message => "succeeded to remove memo" }.to_json
end
